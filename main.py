from cryptoFunctions import *

while True:
    print_dashboard()
    print_menu()
    choice = input("Enter your choice [1-5]: ")
    if choice == '1':
        print_exchanges()
        choice_sub = input("Select Exchange Company:")
        if choice_sub == '1':
            print_cryptos("Paribu")
            choice_sub_2 = input("Select Crypto:")
            if choice_sub_2 == '1':
                addCrypto_dashboard("BTC_TL","Paribu")
            if choice_sub_2 == '2':
                addCrypto_dashboard("ETH_TL", "Paribu")
            if choice_sub_2 == '3':
                addCrypto_dashboard("XRP_TL", "Paribu")
            if choice_sub_2 == '4':
                addCrypto_dashboard("USDT_TL", "Paribu")

        if choice_sub == '2':
            print_cryptos("Koineks")
            choice_sub_2 = input("Select Crypto:")
            if choice_sub_2 == '1':
                addCrypto_dashboard("BTC","Koineks")
            if choice_sub_2 == '2':
                addCrypto_dashboard("ETH", "Koineks")
            if choice_sub_2 == '3':
                addCrypto_dashboard("XRP", "Koineks")
            if choice_sub_2 == '4':
                addCrypto_dashboard("USDT", "Koineks")

    if choice == '2':
        deleteCrypto_dashboard()

    if choice == '3':
        arbitrageCatcher_exchangemenu()
        choice_sub = input("Select Exchanges:")
        if choice_sub == '1':
            arbitrageCatcher_menu()
            choice_sub_2 = input("Select Crypto:")
            if choice_sub_2 == '1':
                checkArbitrage("Paribu", "BTC_TL", "Koineks", "BTC")
            if choice_sub_2 == '2':
                checkArbitrage("Paribu", "ETH_TL", "Koineks", "ETH")
            if choice_sub_2 == '3':
                checkArbitrage("Paribu", "XRP_TL", "Koineks", "XRP")
            if choice_sub_2 == '4':
                checkArbitrage("Paribu", "USDT_TL", "Koineks", "USDT")

    if choice == '4':
        veriler = [checkLast_Values("Paribu","BTC_TL"),
                   checkLast_Values("Paribu","ETH_TL"),
                   checkLast_Values("Paribu","XRP_TL"),
                   checkLast_Values("Paribu","USDT_TL"),
                   checkLast_Values("Koineks","BTC"),
                   checkLast_Values("Koineks","ETH"),
                   checkLast_Values("Koineks","XRP"),
                   checkLast_Values("Koineks","USDT")]


        sendTelegram("""
                    Guncel Kripto Verileri
                    Paribu BTC : {} TL
                    Paribu ETH : {} TL
                    Paribu XRP : {} TL
                    Paribu USDT: {} TL
                        
                    Koineks BTC : {} TL
                    Koineks ETH : {} TL
                    Koineks XRP : {} TL
                    Koineks USDT : {} TL
        
        """.format(veriler[0],veriler[1],veriler[2],veriler[3],veriler[4],veriler[5],veriler[6],veriler[7]))

    if choice == '5':
        continue

    if choice == '6':
        break







