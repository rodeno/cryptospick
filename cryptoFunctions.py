import requests
import csv


def sendTelegram(value):
    token = "953319646:AAGAbz_923Qc0QB1pnfOO3K_otpfKTMVB5o" #telegram token
    chat_id = "840211545" #telegram id
    requests.post(url='https://api.telegram.org/bot{0}/sendMessage'.format(token), data={'chat_id': 840211545, 'text': value}).json()

def print_menu():
    print (30 * "-", "MENU", 30 * "-")
    print ("1. Add Crypto to Dashboard")
    print ("2. Clear Crypto Dashboard")
    print ("3. Arbitrage Catcher")
    print ("4. Send Last Crypto Values to Telegram")
    print ("5. Refresh Dashboard")
    print ("6. Exit")
    print (67 * "-")

def print_cryptos(exchangeName):
    print (28 * "-", exchangeName, 29 * "-")
    print ("1. BTC/TL")
    print ("2. ETH/TL")
    print ("3. XRP/TL")
    print ("4. USDT/TL")

def print_exchanges():
    print (28 * "-","EXCHANGES", 29 * "-")
    print ("1. Paribu")
    print ("2. Koineks")

def arbitrageCatcher_menu():
    print (28 * "-", "CURRENCY LIST" , 29 * "-")
    print ("1. BTC/TL")
    print ("2. ETH/TL")
    print ("3. XRP/TL")
    print ("4. USDT/TL")

def arbitrageCatcher_exchangemenu():
    print (28 * "-", "ARBITRAGE LIST" , 29 * "-")
    print ("1. Paribu - Koineks")

def checkArbitrage(exchangeName1,cryptoName,exchangeName2,cryptoName2):
    var1 = float(checkLast_Values(exchangeName1,cryptoName))
    var2 = float(checkLast_Values(exchangeName2,cryptoName2))
    list = [var1,var2]
    for i in range(0,len(list)-1):
        result = max(list) - min(list)

        print("{}'da {} : {} TL ".format(exchangeName1,cryptoName,var1))
        print("{}'da {} : {} TL ".format(exchangeName2, cryptoName2, var2))
        print("Aradaki Fark : {} TL".format(result))
        exit_arbitrage = input("Enter '1' to Exit Arbitrage Menu")
        if exit_arbitrage == '1':
            continue


def print_dashboard():
    print (23 * "-", "CURRENCY DASHBOARD", 23 * "-")
    with open('dashboard.csv', 'r') as file:
        csvReader = csv.reader(file)
        for i in csvReader:
            if i[0] == '1':
                if i[1] == 'Paribu':
                    print(i[1]+i[2],checkLast_Values("Paribu",i[2]))
                if i[1] == 'Koineks':
                    print(i[1] + i[2],checkLast_Values("Koineks", i[2]))


def deleteCrypto_dashboard():
    text = open("dashboard.csv", "r")
    text = ''.join([i for i in text]) \
        .replace("1", "0")
    x = open("dashboard.csv", "w")
    x.writelines(text)
    x.close()

def addCrypto_dashboard(cryptoName,cryptoExchange):
    with open ('dashboard.csv','a') as file:
        writer = csv.writer(file)
        writer.writerow([1,cryptoExchange,cryptoName])


def checkLast_Values(exchangeName,cryptoName):
    if exchangeName == 'Paribu':
        url = requests.get("https://www.paribu.com/ticker")
        jsonURL = url.json()
        lastValue = jsonURL[cryptoName]["last"]
    if exchangeName == 'Koineks':
        url = requests.get("https://www.koineks.com/ticker")
        jsonURL = url.json()
        lastValue = jsonURL[cryptoName]["current"]

    return (lastValue)
